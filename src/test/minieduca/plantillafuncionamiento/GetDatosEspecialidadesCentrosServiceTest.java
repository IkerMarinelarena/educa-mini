package minieduca.plantillafuncionamiento;

import minieduca.plantillafuncionamiento.aplicacion.GetDatosEspecialidadesCentrosService;
import minieduca.plantillafuncionamiento.aplicacion.excepciones.NoHayMateriasDocentesPorEspecialidad;
import minieduca.plantillafuncionamiento.dominio.MateriaDocenteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class GetDatosEspecialidadesCentrosServiceTest {
    @Test
    void error_cuando_no_hay_datos_de_especialedades() {
        MateriaDocenteRepository materiaDocenteRepository = mock(MateriaDocenteRepository.class);
        GetDatosEspecialidadesCentrosService getDatosEspecialidadesCentrosService = new GetDatosEspecialidadesCentrosService(materiaDocenteRepository);

        when(materiaDocenteRepository.getMateriasDocentes()).thenReturn(new ArrayList<>());

        Exception result = Assertions.assertThrowsExactly(NoHayMateriasDocentesPorEspecialidad.class, () -> getDatosEspecialidadesCentrosService.execute(new String[]{}));
        assertEquals("No hay materias por especialidad docente disponibles", result.getMessage());
        verify(materiaDocenteRepository, times(1)).getMateriasDocentes();
    }
}
