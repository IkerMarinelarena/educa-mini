package minieduca.plantillafuncionamiento;

import minieduca.plantillafuncionamiento.dominio.MateriaDocente;
import minieduca.plantillafuncionamiento.infraestructura.repositorios.FileMateriaDocenteRepository;
import minieduca.plantillafuncionamiento.infraestructura.PlantillaLectorJson;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class FileMateriaDocenteRepositoryTest {
    @Test
    void devuelve_materias_docentes_leidas_de_fichero() {
        PlantillaLectorJson lectorJson = mock(PlantillaLectorJson.class);
        FileMateriaDocenteRepository fileMateriaDocenteRepository = new FileMateriaDocenteRepository(lectorJson);

        when(lectorJson.leer()).thenReturn("[]");

        List<MateriaDocente> materiaDocentes = fileMateriaDocenteRepository.getMateriasDocentes();

        assertEquals(new ArrayList<>() ,materiaDocentes);
        verify(lectorJson, times(1)).leer();
    }
}
