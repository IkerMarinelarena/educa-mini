package minieduca.plantillafuncionamiento;

import minieduca.plantillafuncionamiento.infraestructura.PlantillaLectorJson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class GetDatosEspecialidadesCentrosControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private PlantillaLectorJson plantillaLectorJson;

    @Test
    public void error_recogiendo_datos_especialidades_centros_si_la_peticion_es_incorrecta() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getNecesidadesDocentes").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"Error\":\"Peticion incorrecta, centros obligatorios\"}"));
    }

    @Test
    public void error_recogiendo_datos_especialidades_centros_si_la_peticion_es_incorrecta_con_centros() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getNecesidadesDocentes?centros=").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"Error\":\"Peticion incorrecta, centros obligatorios\"}"));
    }

    @Test
    public void error_si_no_hay_materias_por_especialidades() throws Exception {
        when(plantillaLectorJson.leer()).thenReturn("[]");

        mvc.perform(MockMvcRequestBuilders.get("/getNecesidadesDocentes?centros=pamplona1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"Error\":\"No hay materias por especialidad docente disponibles\"}"));
        verify(plantillaLectorJson, times(1)).leer();
    }
}
