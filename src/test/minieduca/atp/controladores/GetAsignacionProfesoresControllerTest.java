package minieduca.atp.controladores;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import minieduca.atp.infraestructura.ATPClient;
import minieduca.atp.infraestructura.LectorJson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.net.http.HttpRequest;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class GetAsignacionProfesoresControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LectorJson lectorJson;

    @MockBean
    private ATPClient atpClient;

    @Test
    public void no_se_asignan_profesores_si_la_peticion_es_incorrecta() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getAsignacionProfesores").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"Error\":\"Peticion incorrecta, centros obligatorios\"}"));
    }

    @Test
    public void no_se_asignan_profesores_si_no_hay_profesores() throws Exception {
        when(lectorJson.leer()).thenReturn("[]");

        mvc.perform(MockMvcRequestBuilders.get("/getAsignacionProfesores?centros=pamplona1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"Error\":\"No hay profesores disponibles\"}"));

        verify(lectorJson, times(1)).leer();
    }

    @Test
    public void no_se_asignan_profesores_si_no_hay_necesidades_docentes() throws Exception {
        JsonArray jsonResponse = (JsonArray) JsonParser.parseString("[]");

        when(atpClient.send(any(HttpRequest.class))).thenReturn(jsonResponse);
        when(lectorJson.leer()).thenReturn("[nombre:profesor]");

        mvc.perform(MockMvcRequestBuilders.get("/getAsignacionProfesores?centros=pamplona1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"Error\":\"No hay centros con necesidades docentes\"}"));

        verify(lectorJson, times(1)).leer();
    }

    @Test
    public void devuelve_error_si_no_hay_necesidades_docentes_en_plantilla_funcionamiento() throws Exception {
        JsonArray jsonResponse = (JsonArray) JsonParser.parseString("[{'Error':'No hay necesidades docentes'}]");

        when(atpClient.send(any(HttpRequest.class))).thenReturn(jsonResponse);
        when(lectorJson.leer()).thenReturn("[nombre:profesor]");

        mvc.perform(MockMvcRequestBuilders.get("/getAsignacionProfesores?centros=pamplona1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"Error\":\"No hay necesidades docentes\"}"));
    }
}