package minieduca.atp.aplicacion;

import minieduca.atp.aplicacion.excepciones.NecesidadesDocentesNoEncontradosException;
import minieduca.atp.aplicacion.excepciones.ProfesoresNoEncontradosException;
import minieduca.atp.dominio.NecesidadesDocentes;
import minieduca.atp.dominio.NecesidadesDocentesRepository;
import minieduca.atp.dominio.Profesor;
import minieduca.atp.dominio.ProfesorRepository;
import minieduca.atp.infraestructura.APIErrorException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class GetAsignacionProfesoresServiceTest {
    private ProfesorRepository profesorRepository;
    private NecesidadesDocentesRepository necesidadesDocentesRepository;
    private GetAsignacionProfesoresService getAsignacionProfesoresService;

    @BeforeEach
    void setUp() {
        profesorRepository = mock(ProfesorRepository.class);
        necesidadesDocentesRepository = mock(NecesidadesDocentesRepository.class);
        getAsignacionProfesoresService = new GetAsignacionProfesoresService(profesorRepository, necesidadesDocentesRepository);
    }

    @Test
    void no_hay_profesores_para_asignar() {
        when(profesorRepository.getProfesores()).thenReturn(new ArrayList<Profesor>());

        Exception result = Assertions.assertThrowsExactly(ProfesoresNoEncontradosException.class, () -> getAsignacionProfesoresService.execute());
        assertEquals("No hay profesores disponibles", result.getMessage());
        verify(profesorRepository, times(1)).getProfesores();
    }

    @Test
    void no_hay_centros_con_necesidades_docentes() throws APIErrorException {
        List<Profesor> profesores = new ArrayList<Profesor>();
        profesores.add(new Profesor());

        when(profesorRepository.getProfesores()).thenReturn(profesores);
        when(necesidadesDocentesRepository.getNecesidadesDocentes()).thenReturn(new ArrayList<NecesidadesDocentes>());

        Exception result = Assertions.assertThrowsExactly(NecesidadesDocentesNoEncontradosException.class, () -> getAsignacionProfesoresService.execute());
        assertEquals("No hay centros con necesidades docentes", result.getMessage());
        verify(profesorRepository, times(1)).getProfesores();
        verify(necesidadesDocentesRepository, times(1)).getNecesidadesDocentes();
    }
}
