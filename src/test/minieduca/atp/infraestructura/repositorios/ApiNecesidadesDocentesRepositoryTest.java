package minieduca.atp.infraestructura.repositorios;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import minieduca.atp.aplicacion.excepciones.ProfesoresNoEncontradosException;
import minieduca.atp.dominio.NecesidadesDocentes;
import minieduca.atp.infraestructura.APIErrorException;
import minieduca.atp.infraestructura.ATPClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ApiNecesidadesDocentesRepositoryTest {
    @Test
    void no_hay_necesidades_docentes() throws APIErrorException {
        ATPClient atpClient = mock(ATPClient.class);
        ApiNecesidadesDocentesRepository apiNecesidadesDocentesRepository = new ApiNecesidadesDocentesRepository(atpClient);
        String jsonString = "[]";
        JsonArray jsonResponse = (JsonArray) JsonParser.parseString(jsonString);

        when(atpClient.send(any(HttpRequest.class))).thenReturn(jsonResponse);

        List<NecesidadesDocentes> necesidadesDocentes =  apiNecesidadesDocentesRepository.getNecesidadesDocentes();

        assertEquals(new ArrayList<NecesidadesDocentes>(), necesidadesDocentes);
    }

    @Test
    void error_no_hay_necesidades_docentes() {
        ATPClient atpClient = mock(ATPClient.class);
        ApiNecesidadesDocentesRepository apiNecesidadesDocentesRepository = new ApiNecesidadesDocentesRepository(atpClient);
        String jsonString = "[{'Error':'No hay necesidades docentes'}]";
        JsonArray jsonResponse = (JsonArray) JsonParser.parseString(jsonString);

        when(atpClient.send(any(HttpRequest.class))).thenReturn(jsonResponse);

        Exception result = Assertions.assertThrowsExactly(APIErrorException.class, apiNecesidadesDocentesRepository::getNecesidadesDocentes);
        assertEquals("No hay necesidades docentes", result.getMessage());
    }
}