package minieduca.atp.infraestructura.repositorios;

import minieduca.atp.dominio.Profesor;
import minieduca.atp.infraestructura.LectorJson;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class JsonProfesorRepositoryTest {
    @Test
    void no_encuentra_ningun_profesor() {
        LectorJson lectorJson = mock(LectorJson.class);
        JsonProfesorRepository jsonProfesorRepository = new JsonProfesorRepository(lectorJson);

        when(lectorJson.leer()).thenReturn("[]");

        List<Profesor> profesores = jsonProfesorRepository.getProfesores();

        verify(lectorJson, times(1)).leer();
        assertEquals(new ArrayList<Profesor>(), profesores);
    }
}