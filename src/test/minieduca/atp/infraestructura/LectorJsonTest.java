package minieduca.atp.infraestructura;

import com.google.common.collect.ImmutableList;
import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class LectorJsonTest {

    @Disabled("Pendiente de manejo de ficheros en java")
    @Test
    void lee_fichero_json() throws IOException {
        FileSystem fileSystem = Jimfs.newFileSystem(Configuration.unix());
        Path path = fileSystem.getPath("classpath:disponibilidad_profesores.json");
        Files.createDirectory(path);
        Path disponibilidadProfesores = path.resolve("disponibilidad_profesores.json");

        Files.write(disponibilidadProfesores, ImmutableList.of("{name: name}"), StandardCharsets.UTF_8);
        Path resourceFilePath = fileSystem.getPath("disponibilidad_profesores.json");

        LectorJson lectorJson = new LectorJson(new DefaultResourceLoader());

        String profesoresDisponibles = lectorJson.leer();

        assertEquals("{name: name}", profesoresDisponibles);
    }
}
