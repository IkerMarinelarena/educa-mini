package minieduca.plantillafuncionamiento.infraestructura.repositorios;

import minieduca.plantillafuncionamiento.dominio.MateriaDocente;
import minieduca.plantillafuncionamiento.dominio.MateriaDocenteRepository;
import minieduca.plantillafuncionamiento.infraestructura.PlantillaLectorJson;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FileMateriaDocenteRepository implements MateriaDocenteRepository {
    private final PlantillaLectorJson lectorJson;

    public FileMateriaDocenteRepository(PlantillaLectorJson lectorJson) {
        this.lectorJson = lectorJson;
    }

    @Override
    public List<MateriaDocente> getMateriasDocentes() {
        String lecturaMateriasDocentes = lectorJson.leer();

        if (lecturaMateriasDocentes.equals("[]")){
            return new ArrayList<>();
        }

        return null;
    }
}
