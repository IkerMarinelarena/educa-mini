package minieduca.plantillafuncionamiento.infraestructura.controladores;

import minieduca.plantillafuncionamiento.aplicacion.GetDatosEspecialidadesCentrosService;
import minieduca.plantillafuncionamiento.aplicacion.excepciones.NoHayMateriasDocentesPorEspecialidad;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Map;

@RestController
public class GetDatosEspecialidadesCentrosController {
    private GetDatosEspecialidadesCentrosService getDatosEspecialidadesCentrosService;

    public GetDatosEspecialidadesCentrosController(GetDatosEspecialidadesCentrosService getDatosEspecialidadesCentrosService) {
        this.getDatosEspecialidadesCentrosService = getDatosEspecialidadesCentrosService;
    }

    @GetMapping("/getNecesidadesDocentes")
    public ResponseEntity<Object> index(@RequestParam(value = "centros", required = false) String[] centros) {
        if (centros == null ||  Arrays.stream(centros).toList().isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(Map.of("Error", "Peticion incorrecta, centros obligatorios"));
        };

        try {
            getDatosEspecialidadesCentrosService.execute(centros);
        } catch (NoHayMateriasDocentesPorEspecialidad noHayMateriasDocentesPorEspecialidad) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(Map.of("Error", noHayMateriasDocentesPorEspecialidad.getMessage()));
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body("ok");
    }
}