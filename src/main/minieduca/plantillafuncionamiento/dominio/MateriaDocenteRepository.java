package minieduca.plantillafuncionamiento.dominio;

import minieduca.plantillafuncionamiento.dominio.MateriaDocente;

import java.util.List;

public interface MateriaDocenteRepository {
    List<MateriaDocente> getMateriasDocentes();
}
