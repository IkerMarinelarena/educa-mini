package minieduca.plantillafuncionamiento.aplicacion.excepciones;

public class NoHayMateriasDocentesPorEspecialidad extends Exception {
    public NoHayMateriasDocentesPorEspecialidad() {
        super("No hay materias por especialidad docente disponibles");
    }
}
