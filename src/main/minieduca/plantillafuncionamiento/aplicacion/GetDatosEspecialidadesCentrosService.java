package minieduca.plantillafuncionamiento.aplicacion;

import minieduca.plantillafuncionamiento.aplicacion.excepciones.NoHayMateriasDocentesPorEspecialidad;
import minieduca.plantillafuncionamiento.dominio.MateriaDocente;
import minieduca.plantillafuncionamiento.dominio.MateriaDocenteRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetDatosEspecialidadesCentrosService {
    private final MateriaDocenteRepository materiaDocenteRepository;

    public GetDatosEspecialidadesCentrosService(MateriaDocenteRepository materiaDocenteRepository) {
        this.materiaDocenteRepository = materiaDocenteRepository;
    }

    public void execute(String[] centros) throws NoHayMateriasDocentesPorEspecialidad {
        List<MateriaDocente> materiasDocentes = materiaDocenteRepository.getMateriasDocentes();
        if (materiasDocentes.isEmpty()){
            throw new NoHayMateriasDocentesPorEspecialidad();
        }
    }
}
