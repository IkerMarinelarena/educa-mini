package minieduca.atp.dominio;

import minieduca.atp.dominio.Profesor;

import java.util.List;

public interface ProfesorRepository {
    List<Profesor> getProfesores();
}
