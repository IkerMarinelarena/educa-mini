package minieduca.atp.dominio;

import minieduca.atp.dominio.NecesidadesDocentes;
import minieduca.atp.dominio.Profesor;
import minieduca.atp.infraestructura.APIErrorException;

import java.util.List;

public interface NecesidadesDocentesRepository {
    List<NecesidadesDocentes> getNecesidadesDocentes() throws APIErrorException;
}
