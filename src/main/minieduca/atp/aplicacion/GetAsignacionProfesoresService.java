package minieduca.atp.aplicacion;

import minieduca.atp.aplicacion.excepciones.NecesidadesDocentesNoEncontradosException;
import minieduca.atp.aplicacion.excepciones.ProfesoresNoEncontradosException;
import minieduca.atp.dominio.NecesidadesDocentes;
import minieduca.atp.dominio.NecesidadesDocentesRepository;
import minieduca.atp.dominio.Profesor;
import minieduca.atp.dominio.ProfesorRepository;
import minieduca.atp.infraestructura.APIErrorException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAsignacionProfesoresService {
    private final ProfesorRepository profesorRepository;
    private final NecesidadesDocentesRepository necesidadesDocentesRepository;

    public GetAsignacionProfesoresService(ProfesorRepository profesorRepository, NecesidadesDocentesRepository necesidadesDocentesRepository) {
        this.profesorRepository = profesorRepository;
        this.necesidadesDocentesRepository = necesidadesDocentesRepository;
    }

    public void execute() throws ProfesoresNoEncontradosException, NecesidadesDocentesNoEncontradosException, APIErrorException {
        List<Profesor> profesores = profesorRepository.getProfesores();
        if (profesores.isEmpty()) {
            throw new ProfesoresNoEncontradosException();
        }

        List<NecesidadesDocentes> necesidadesDocentes = this.necesidadesDocentesRepository.getNecesidadesDocentes();
        if(necesidadesDocentes.isEmpty()){
            throw new NecesidadesDocentesNoEncontradosException();
        }
    }
}