package minieduca.atp.aplicacion.excepciones;

public class ProfesoresNoEncontradosException extends Exception{
    public ProfesoresNoEncontradosException() {
        super("No hay profesores disponibles");
    }
}
