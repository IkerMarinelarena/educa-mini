package minieduca.atp.aplicacion.excepciones;

public class NecesidadesDocentesNoEncontradosException extends Exception{
    public NecesidadesDocentesNoEncontradosException() {
        super("No hay centros con necesidades docentes");
    }
}
