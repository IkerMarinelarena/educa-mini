package minieduca.atp.infraestructura.controladores;

import minieduca.atp.aplicacion.GetAsignacionProfesoresService;
import minieduca.atp.aplicacion.excepciones.NecesidadesDocentesNoEncontradosException;
import minieduca.atp.aplicacion.excepciones.ProfesoresNoEncontradosException;
import minieduca.atp.infraestructura.APIErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Map;

@RestController
public class GetAsignacionProfesoresController {
    private final GetAsignacionProfesoresService getAsignacionProfesoresService;

    public GetAsignacionProfesoresController(GetAsignacionProfesoresService getAsignacionProfesoresService) {
        this.getAsignacionProfesoresService = getAsignacionProfesoresService;
    }

    @GetMapping(path = "/getAsignacionProfesores")
    public ResponseEntity<Object> index(@RequestParam(value = "centros", required = false) String[] centros) {
        if (centros == null || Arrays.stream(centros).toList().isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(Map.of("Error", "Peticion incorrecta, centros obligatorios"));
        }
        ;

        try {
            getAsignacionProfesoresService.execute();
        } catch (APIErrorException | ProfesoresNoEncontradosException | NecesidadesDocentesNoEncontradosException excepcion) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(Map.of("Error", excepcion.getMessage()));
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body("ok");
    }
}