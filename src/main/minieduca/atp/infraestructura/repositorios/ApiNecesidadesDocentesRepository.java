package minieduca.atp.infraestructura.repositorios;

import com.google.gson.JsonArray;
import minieduca.atp.dominio.NecesidadesDocentesRepository;
import minieduca.atp.dominio.NecesidadesDocentes;
import minieduca.atp.infraestructura.APIErrorException;
import minieduca.atp.infraestructura.ATPClient;
import org.springframework.stereotype.Repository;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ApiNecesidadesDocentesRepository implements NecesidadesDocentesRepository {
    private final ATPClient atpClient;

    public ApiNecesidadesDocentesRepository(ATPClient atpClient) {
        this.atpClient = atpClient;
    }

    @Override
    public List<NecesidadesDocentes> getNecesidadesDocentes() throws APIErrorException {
        HttpRequest request = HttpRequest
                .newBuilder(URI.create("http://localhost:8080/getNecesidadesDocentes?centros="))
                .header("accept", "application/json")
                .build();

        JsonArray respuestaNecesidadesDocentes = atpClient.send(request);

        List<NecesidadesDocentes> necesidadesDocentes = new ArrayList<NecesidadesDocentes>();
        if (respuestaNecesidadesDocentes != null || !respuestaNecesidadesDocentes.isEmpty()){
            if(respuestaNecesidadesDocentes.toString().contains("Error")){
                throw new APIErrorException("No hay necesidades docentes");
            }
        }

        return necesidadesDocentes;
    }
}
