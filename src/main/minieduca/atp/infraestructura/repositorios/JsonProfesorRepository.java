package minieduca.atp.infraestructura.repositorios;

import minieduca.atp.dominio.ProfesorRepository;
import minieduca.atp.dominio.Profesor;
import minieduca.atp.infraestructura.LectorJson;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class JsonProfesorRepository implements ProfesorRepository {
    private final LectorJson lectorJson;

    public JsonProfesorRepository(LectorJson lectorJson) {
        this.lectorJson = lectorJson;
    }

    @Override
    public List<Profesor> getProfesores() {
        String lecturaProfesores = lectorJson.leer();
        List<Profesor> profesores = new ArrayList<>();
        if (lecturaProfesores.equals("[]")){
            return profesores;
        }
        profesores.add(new Profesor());

        return profesores;
    }
}
