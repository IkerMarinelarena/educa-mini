package minieduca.atp.infraestructura;

public class APIErrorException extends Exception{
    public APIErrorException(String errorMessage) {
        super(errorMessage);
    }
}
